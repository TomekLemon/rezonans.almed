<?php 
    require 'config.php';
?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <meta charset="utf-8">
        <title>Rezonans Magnetyczny - Pracownie ALMED w Gorlicach i w Przeworsku</title>
		<meta name="description" content="Rezonans magnetyczny Almed" />
        <link rel="stylesheet" href="<?php echo assets(); ?>/css/default.min.css<?php echo versioning()?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="icon" type="image/png" href="favicon.png<?php echo versioning()?>">
        <script defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCuznpSzv43UdnGkZIIbSSyuMWa8nsugdk&callback=initMap">
        </script>
    </head>
    <body>
        <div class="upper-section clearfix">
            <div class="container">
                <p class="pull-left phone-no">
                    Gorlice <i class="icon-phone" aria-hidden="true"></i> <a href="tel:722210186">722 210 186</a> 
                </p>
                <p class="pull-right phone-no">
                    Przeworsk <i class="icon-phone" aria-hidden="true"></i> <a href="tel:722210962">722 210 962</a>
                </p>
            </div>
        </div>
        <header class="header">
            <div class="brand-section clearfix">
                <div class="pull-left brand"><a href=""><h1>Rezonans Almed</h1></a></div>
                <a href="#" class="nav-collapse-btn" id="nav-trigger">Menu</a>
                <div class="upper-nav pull-right">
                    <?php include 'partials/_menu.html'; ?>
                </div>
            </div>
        </header>
        <div class="slider-section" id="witamy">
            <ul class="unstyled inline cycle-slideshow"
                    data-cycle-fx="scrollHorz"
                    data-cycle-speed="700"
                    data-cycle-timeout="7000"
                    data-cycle-slides="li"
                    data-cycle-swipe="true"
                    data-cycle-swipe-fx=scrollHorz
                >
                <li style="background: url('<?php echo assets(); ?>/images/slider1.jpg<?php echo versioning()?>') no-repeat;">
                    <div class="container">
                        <div class="slider-text" style="background: url('<?php echo assets(); ?>/images/slider1-text.png<?php echo versioning()?>') no-repeat">
                            <div class="header-container">
                                <h1>Rezonans magnetyczny</h1>
                            </div>
                        </div>
                    </div>
                </li>
                <li style="background: url('<?php echo assets(); ?>/images/slider2.jpg<?php echo versioning()?><?php echo versioning()?>') no-repeat;">
                    <div class="container">
                        <div class="slider-text" style="background: url('<?php echo assets(); ?>/images/slider2-text.png<?php echo versioning()?>') no-repeat">
                            <div class="header-container">
                                <h1>Rezonans magnetyczny</h1>
                        </div>
                        </div>
                    </div>
                </li>
                <li style="background: url('<?php echo assets(); ?>/images/slider3.jpg<?php echo versioning()?><?php echo versioning()?>') no-repeat;">
                    <div class="container">
                        <div class="slider-text" style="background: url('<?php echo assets(); ?>/images/slider3-text.png<?php echo versioning()?>') no-repeat">
                            <div class="header-container">
                                <h1>Wysokopolowy rezonans magnetyczny</h1>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="main-content" id="pracownie">
            <div class="container">
                <div class="section clearfix">
                    <div class="left-col">
                        <h2 class="section-title bolded">Gorlice</h2>
                        <div class="text">
                            <p>
                                ALMED Pracownia Rezonansu Magnetycznego w Gorlicach została wyposażona w wysokiej klasy rezonans magnetyczny Signa HDxt renomowanego producenta GE Healthcare o sile pola magnetycznego 1,5 Tesli.
                            </p>
                            <p>
                                Nasz Rezonans Magnetyczny w trakcie badania może wykorzystywać do 16 kanałów zbierających dane, dzięki czemu przy zachowaniu najwyższej jakości możliwe jest skrócenie czasu badania, a wykonanie niektórych badań staje się w ogóle możliwe.
                            </p>
                            <p>
                                Dzięki platformie HD rezonans magnetyczny pozwala na obrazowanie ludzkiego ciała z najwyższą dokładnością, co pomaga naszym lekarzom radiologom w dokładnej diagnostyce schorzeń naszych pacjentów. Pracownia zlokalizowana jest w budynku przychodni Szpitala Specjalistycznego im. Henryka Klimontowicza w Gorlicach, z możliwością dojazdu bezpośrednio pod pracownię dla pacjentów szpitalny leżących czy osób niepełnosprawnych. Dla poprawienia komfortu naszych pacjentów istnieje możliwość słuchania muzyki w trakcie badania.
                            </p>
                            <p>
                                Zapraszamy już dziś na wykonanie badania rezonansu magnetycznego MR w naszej pracowni.
                            </p>
                        </div>
                    </div>
                    <div class="right-col">
                        <h2 class="section-title bolded">Przeworsk</h2>
                        <div class="text">
                            <p>Pracownia Rezonansu Magnetycznego w Przeworsku to przede wszystkim doświadczony zespół personelu medycznego, który od 10 lat stale podnosi swoje kwalifikacje w zakresie diagnostyki obrazowej. Początkowo w zakresie tomografii komputerowej, a następnie rezonansu magnetycznego. Obecnie w pracowni funkcjonuje nowoczesny wysokopolowy 1,5 T rezonans magnetyczny Magnetom Avanto uznanego producenta sprzętu medycznego Siemens Healthcare. </p>
                            <p>Zainstalowany rezonans korzysta z 18 kanałów zbierających dane i wielokanałowych cewek przeznaczonych do badania wszystkich części anatomicznych człowieka. Dzięki zastosowaniu technologii TIM możliwe jest przebadanie bardzo dużego obszaru pacjenta bez konieczności zmiany pozycji czy cewek. Aparat z „krótki” magnesem umożliwia badanie pacjentów cierpiących na klaustrofobię oraz dzieci. Pracowni zlokalizowana jest w Szpitalu Powiatowym im. Jana Pawła II w Przeworsku z możliwością dojazdu pod samą pracownie zarówno dla pacjentów szpitalnych jak i o utrudnionym poruszaniu się.</p>
                            <p>Zapraszamy !</p>
                        </div>
                    </div>
                </div>
                <div class="section clearfix">
                    <div class="gallery">
                        <ul class="inline unstyled">
                            <li>
                                <div class="img-container">
                                    <a href="<?php echo assets(); ?>/images/galeria/1.jpg<?php echo versioning()?>" class="colorbox" rel="next">
                                        <img src="<?php echo assets(); ?>/images/galeria/1m.jpg<?php echo versioning()?>" alt="Rezonans Almed" />
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="img-container">
                                    <a href="<?php echo assets(); ?>/images/galeria/2.jpg<?php echo versioning()?>" class="colorbox" rel="next">
                                        <img src="<?php echo assets(); ?>/images/galeria/2m.jpg<?php echo versioning()?>" alt="Rezonans Almed" />
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="img-container">
                                    <a href="<?php echo assets(); ?>/images/galeria/3.jpg<?php echo versioning()?>" class="colorbox" rel="next">
                                        <img src="<?php echo assets(); ?>/images/galeria/3m.jpg<?php echo versioning()?>" alt="Rezonans Almed" />
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="img-container">
                                    <a href="<?php echo assets(); ?>/images/galeria/4.jpg<?php echo versioning()?>" class="colorbox" rel="next">
                                        <img src="<?php echo assets(); ?>/images/galeria/4m.jpg<?php echo versioning()?>" alt="Rezonans Almed" />
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="img-container">
                                    <a href="<?php echo assets(); ?>/images/galeria/5.jpg<?php echo versioning()?>" class="colorbox" rel="next">
                                        <img src="<?php echo assets(); ?>/images/galeria/5m.jpg<?php echo versioning()?>" alt="Rezonans Almed" />
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="img-container">
                                    <a href="<?php echo assets(); ?>/images/galeria/6.jpg<?php echo versioning()?>" class="colorbox" rel="next">
                                        <img src="<?php echo assets(); ?>/images/galeria/6m.jpg<?php echo versioning()?>" alt="Rezonans Almed" />
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="img-container">
                                    <a href="<?php echo assets(); ?>/images/galeria/7.jpg<?php echo versioning()?>" class="colorbox" rel="next">
                                        <img src="<?php echo assets(); ?>/images/galeria/7m.jpg<?php echo versioning()?>" alt="Rezonans Almed" />
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="img-container">
                                    <a href="<?php echo assets(); ?>/images/galeria/8.jpg<?php echo versioning()?>" class="colorbox" rel="next">
                                        <img src="<?php echo assets(); ?>/images/galeria/8m.jpg<?php echo versioning()?>" alt="Rezonans Almed" />
                                    </a>
                                </div>
                            </li>
                            <li>
                                <div class="img-container">
                                    <a href="<?php echo assets(); ?>/images/galeria/9.jpg<?php echo versioning()?>" class="colorbox" rel="next">
                                        <img src="<?php echo assets(); ?>/images/galeria/9m.jpg<?php echo versioning()?>" alt="Rezonans Almed" />
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="section clearfix" id="zakres">
                <div class="container">
                    <div class="left-col zakres">
                        <h2 class="section-title">ZAKRES BADANIA</h2>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Rezonans Magnetyczny głowy i mózgowia 
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        Badanie MR wykonywane w przypadku chorób naczyniowych (udarowych), zmian niedokrwiennych mózgu, diagnostyce guzów, stwardnienia rozsianego, wad rozwojowych, chorób zwyrodnieniowych i degeneracyjnych jak choroba Alzheimera, schorzeń zapalnych ośrodkowego układu nerwowego
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                           Rezonans Magnetyczny przysadki mózgowej 
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        Badanie MR wykonywane w celu uwidocznienia lub wykluczenia mikrogruczolaków, guza pierwotnego okolicy wewnątrzsiodłowej czy okołosiodłowej, poszukiwania procesów przerzutowych
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Rezonans Magnetyczny kręgosłupa 
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        Badanie MR wykonywane osobno w odcinkach (szyjnego, piersiowego i lędźwiowo-krzyżowego), kanału kręgowego i rdzenia – badanie wykonuje się w diagnostyce schorzeń powstałych w wyniku zmian pourazowych i pooperacyjnych, diagnostyce nacieków lub przerzutów nowotworowych, chorób zwyrodnieniowych kręgosłupa, oceny patologii krążka międzykręgowego, dyskopatii, malformacji naczyniowych, wad rozwojowych
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapseThree">
                                           Rezonans Magnetyczny stawu 
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        Badanie MR stawu m.in. barkowego, łokciowego, nadgarstka, biodrowego, kolanowego, skokowego – wykonywane w celu dokładnej oceny stanu chrząstek, wiązadeł i ścięgien, ocena uszkodzenia łękotek w stawach kolanowych, obecności płynu w jamie stawu, zmian zapalnych, uszkodzenia ścięgien o charakterze przewlekłym jak i ostrym.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFiv">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapseThree">
                                          Rezonans Magnetyczny jamy brzusznej 
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        Badanie MR stosowane w diagnostyce chorób wątroby i dróg żółciowych, chorób trzustki, jelita cienkiego i grubego, chorób układu moczowego, rozszerzone o badanie miednicy pozwala na ocenę zmian w narządzę rodnym czy gruczole krokowym.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingSix">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapseThree">
                                           Rezonans Magnetyczny - Cholangiopankreatografia MR (MRCP)
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        Badanie MR pozwalające na uwidocznienie obrazów pęcherzyka żółciowego, dróg żółciowych i trzustkowych, dzięki czemu możliwa jest ocena zawartości pęcherzyka żółciowego, dróg żółciowych i przewodu trzustkowego oraz ocena stosunku anatomicznego w ich okolicy, metoda ta pozwala rozpoznać obecność złogów, zwężeń pozapalnych, przewlekłe zapalenie trzustki oraz zmiany nowotworowe.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingSeven">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapseThree">
                                          Rezonans Magnetyczny - Angio MR i Angiografia MR  
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        Badanie MR wykorzystywane w celu uwidocznienia naczyń tętniczych i żylnych, przede wszystkim w celu oceny ich drożności, badanie wykorzystuje się w celu diagnostyki tętniaków, malformacji tętniczo-żylnych, naczyniaków żylnych oraz anomalii rozwojowych.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <p>* Badanie w zależności od wskazań wykonywane jest bez lub z podaniem środka cieniującego.</p>
                    </div>
                    <div class="right-col przebieg" id="przebieg">
                        <h2 class="section-title">PRZEBIEG BADANIA</h2>
                        <ul class="nav nav-tabs">
                            <li class="active" ><a data-toggle="tab" href="#sectionA">&bull; Rejestracja</a></li>
                            <li ><a data-toggle="tab" href="#sectionB">&bull;&bull; Podanie kontrastu</a></li>
                            <li ><a data-toggle="tab" href="#sectionC">&bull;&bull;&bull; Pokój badań</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="sectionA" class="tab-pane fade in active">
                                <p>Przed badaniem prosimy o zgłoszenie się do rejestracji w celu przekazania skierowania I wyników badań, asystująca pielęgniarka przeprowadzi dodatkowo ankietę dotyczącą Twojego stanu zdrowia,</p>
                                <p>Następnie zostaniesz poproszona(y) o przejście do przebieralni, gdzie możesz przebrać się w luźne ciuchy. Pamiętaj o pozostawieniu wszelkich metalowych przedmiotów (zgodnie z informacją o przygotowaniu ).</p>
                            </div>
                            <div id="sectionB" class="tab-pane fade">
                                <p>Jeżeli badanie będzie wymagało podania środka kontrastowego, asystująca pielęgniarka, dokona dożylnego wkłucia kaniuli (wenflonu). Jeszcze raz sprawdź czy na ciele lub przy sobie nie znajdują się jakiekolwiek przedmioty metalowe.</p>
                            </div>
                            <div id="sectionC" class="tab-pane fade">
                                <p>Zostaniesz poproszony o przejście do pokoju badań. W pokoju badań znajduje się rezonans magnetyczny, zgodnie z instrukcjami technika wykonującego badanie połóż się na stole. Zachowaj w miarę możliwości wygodną pozycję, będziesz musiał w niej wytrzymać w bezruchu, do końca badania.</p>
                                <p>Jeżeli będzie to możliwe z uwagi na rodzaj badania technik założy słuchawki umożliwiające słuchanie muzyki podczas badania i zmniejszające w ten sposób dźwięk rezonansu . Badanie rozpocznie się wsunięciem stołu w głąb magnesu i będzie trwać ok 15 do 30 minut.</p>
                                <p>W trakcie badania technik może podawać instrukcje jak: “proszę nie oddychać”, “można oddychać”, to od współpracy z technikiem zależy w znacznym stopniu jakość badania. Zaczynamy.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        <div class="offset-parent" id="przygotowanie" >
            <div class="section clearfix">
                <div class="container">
                <div class="left-col przygotowanie">
                    <h2 class="section-title">PRZYGOTOWANIE DO BADANIA</h2>
                    <ul class="unstyled">
                        <li>
                            <div class="icon-container">
                                <i class="icon-info-circled"></i>
                            </div>
                            <div class="text-container">
                                <h3 class="icon-header">Zapoznaj się z przeciwskazaniami</h3>
                                <p>
                                    Zapoznaj się z przeciwwskazaniami do wykonania badania rezonansu magnetycznygo
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="icon-container">
                                <i class="icon-doc-text"></i>
                            </div>
                            <div class="text-container">
                                <h3 class="icon-header">PRZYGOTUJ SKIEROWANIE I WYNIKI</h3>
                                <p>
                                    Rezonans Magnetyczny – Badanie MR, jest badaniem precyzyjnym, którego poszczególne sekwencje uzależnia się od podania przez lekarza kierującego informacji o podejrzeniu/spodziewanym różnicowaniu, dlatego w celu prawidłowego wykonania badania, wymagane jest dostarczenie skierowania na badanie.
                                </p>
                                <p>
                                    Niezwykle pomocnym przy ocenie badania rezonansu magnetycznego są wyniki wcześniej wykonanych badań RTG, USG, tomografii komputerowej czy rezonansu magnetycznego, dodatkowo uzupełniają one wiedzę lekarza radiologa oceniającego badanie, dlatego w miarę możliwości prosimy o ich dostarczenie przed badaniem.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="icon-container">
                                <i class="icon-book-open"></i>
                            </div>
                            <div class="text-container">
                                <h3 class="icon-header">WYKONAJ BADANIA LABORATORYJNE KREATYNINA I GFR</h3>
                                <p>
                                    Z uwagi na możliwość/konieczność wykonania badania rezonansu magnetycznego z zastosowaniem środków cieniujących, warz ze skierowaniem prosimy dostarczyć aktualny wynik badania kreatyniny we krwi (30 dni), a w przypadku pacjentów leczących się z powodu niewydolności nerek, wątroby lub po przeszczepie narządu, dodatkowo aktualny w dniu badania wynik GFR.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="icon-container">
                                <i class="icon-user-add"></i>
                            </div>
                            <div class="text-container">
                                <h3 class="icon-header">PRZYGOTOWANIE WŁAŚCIWE</h3>
                                <p>
                                    Dla badań rezonansu magnetycznego z podaniem środka cieniującego – Pacjent powinien wypić ok 2 litry wody, na dzień przed badaniem i 1 litra wody, słabej herbaty w dniu badania. Na badanie pacjent powinien się zgłosić na czczo ( 3 godziny bez jedzenia, picia, palenia papierosów ).
                                </p>
                                <p>
                                    Ważne jest, by do badania przystąpić w luźnym ubraniu pozbawionym w szczególności metalowych elementów takich jak: guziki, pasek, zegarek, klucze, karty płatnicze i telefon komórkowy (mogą ulec rozmagnesowaniu).
                                </p>
                                <p>
                                    Nie można mieć na sobie kolczyków, broszek, naszyjników, zegarków naręcznych. Zaleca się zdjęcie usuwalnych protez dentystycznych. W przypadku badania głowy i oczodołów nie robimy makijażu i rezygnujemy z lakieru do włosów. Kosmetyki zawierają drobinki metali, które fałszują wyniki.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="right-col przeciwskazania">
                    <h2 class="section-title">PRZECIWWSKAZANIA DO BADANIA</h2>
                    <ul class="unstyled">
                        <li>
                            <div class="icon-container">
                                <i class="icon-block"></i>
                            </div>
                            <div class="text-container">
                                <h3 class="icon-header">BEZWZGLĘDNE PRZECIWSKAZANIA</h3>
                                <p>
                                    Rezonans Magnetyczny wytwarza bardzo silne pole magnetyczne ! i tym samym oddziałuje na urządzenia elektroniczne i elektryczne, dlatego bezwzględnymi przeciwwskazaniami do wykonania badania są wszczepione u pacjenta urządzenia elektryczne i elektroniczne, a w szczególności:
                                </p>
                                <ul class="block">
                                    <li>układ stymulujący pracę serca</li>
                                    <li>Pompa insulinowa</li>
                                    <li>Wszczepiony aparat słuchowy</li>
                                    <li>Neurostymulatory</li>
                                    <li>Klipsy metalowe wewnątrzczaszkowe</li>
                                    <li>Ciało metaliczne w oku</li>
                                </ul>
                            </div>
                            </li>
                        <li>
                            <div class="icon-container">
                                <i class="icon-warning"></i>
                            </div>
                            <div class="text-container">
                                <h3 class="icon-header">WZGLĘDNE PRZECIWWSKAZANIA</h3>
                                <p>
                                    Względnymi przeciwskazaniami do wykonywania badania rezonansu magnetycznego są:
                                </p>
                                <ul class="block">
                                    <li>Klaustrofobia</li>
                                    <li>Metalowe ciała obce w tkankach miękkich</li>
                                    <li>Implanty ortopedyczne i inne</li>
                                    <li>Sztuczne zastawki serca</li>
                                    <li>Trwały tatuaż, makijaż</li>
                                    <li>Protezy implanty stomatologiczne</li>
                                    <li>Ciąża – niewskazane wykonywanie w 1 trymestrze</li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <div class="icon-container">
                                <i class="icon-lamp"></i>
                            </div>
                            <div class="text-container">
                                <h3 class="icon-header">UWAGA NA WSZCZEPIONE IMPLANTY </h3>
                                <p>
                                   Badania Rezonansu Magnetycznego u pacjentów z wszczepionymi implantami, stanowiącymi względne przeciwskazanie do jego wykonania, mogą być wykonywane pod warunkiem otrzymania od lekarza, który dokonał wszczepienia implantu lub lekarza kierującego zaświadczenia, że wszczepiony implant nie stanowi przeciwskazania do wykonania badania na rezonansie magnetycznym o sile pola 1,5 T. Możliwe jest również przekazanie do naszej pracowni oświadczenia producenta implantu w tym zakresie.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        </div>
        <div class="section clearfix maps" id="kontakt">
            <div class="container">
                <div class="left-col">
                    <h2 class="section-title">Dojazd do pracownii rezonansu magnetycznego w Gorlicach</h2>
                    <div class="embed-responsive-16by9" id="gorlice"></div>
                </div>
                <div class="right-col">
                    <h2 class="section-title">Dojazd do pracownii rezonansu magnetycznego w Przeworsku</h2>
                    <div class="embed-responsive-16by9" id="przeworsk"></div>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <ul class="unstyled inline">
                    <li>
                        <h3 class="footer-title">PRACOWNIA W GORLICACH</h3>
                        <p>Almed Pracownia Resonansu<br />Magnetycznego w Gorlicach</p>
                        <ul class="unstyled block-list">
                            <li><i class="icon-home"></i> ul. Węgierska 21 38-300 Gorlice</li>
                            <li><i class="icon-phone"></i> <a href="tel:722210186">722 210 186</a></li>
                            <li><i class="icon-mail-alt"></i> <a href="mailto:gorlice@almed.com.pl">gorlice@almed.com.pl</a></li>
                        </ul>
                    </li>
                    <li>
                        <h3 class="footer-title">NASZ REZONANS MAGNETYCZNY – FAKTY:</h3>
                        <ul class="styled block-list">
                            <li>wysoka klasa urządzenia</li>
                            <li>wykwalifikowany personel</li>
                            <li>możliwie kr&oacute;tki czas oczekiwania</li>
                            <li>zapis pełnego badania na CD</li>
                            <li>dogodny dojazd dla osób niepełnosprawnych</li>
                        </ul>
                    </li>
                    <li>                        
                        <h3 class="footer-title">DO POBRANIA</h3>
                        <div class="icon">
                            <i class="icon-angle-right"></i>
                            <a href="<?php echo assets(); ?>/download/skierowanie.pdf" target="_blank"> skierowanie do Gorlic</a>
                        </div>
                        <div class="icon">
                            <i class="icon-angle-right"></i>
                            <a href="<?php echo assets(); ?>/download/ankieta.pdf" target="_blank"> ankieta</a>
                        </div>
                    </li>

                    <li>
                        <h3 class="footer-title">PRACOWNIA W PRZEWORSKU</h3>
                        <p>Almed Pracownia Resonansu<br />Magnetycznego w Przeworsku</p>
                        <ul class="unstyled block-list">
                            <li><i class="icon-home"></i> ul. Szpitalna 16 37-200 Przeworsk</li>
                            <li><i class="icon-phone"></i> <a href="tel:722210962">722 210 962</a></li>
                            <li><i class="icon-mail-alt"></i> <a href="mailto:przeworsk@almed.com.pl">przeworsk@almed.com.pl</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </footer>
        <div class="copyright clearfix">
            <div class="container">
                <div class="pull-left">
                    &copy; Copyright 2017 - ALMED Sp. z o.o.
                </div>
                <div class="pull-right">
                    <ul class="unstyled inline">
                        <li>
                            <a href="#">
                                <i class="icon-facebook" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class="icon-gplus" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
       <link href="https://fonts.googleapis.com/css?family=Asap:400,500,700;subset=latin-ext" rel="stylesheet">
       <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:700&amp;subset=latin-ext" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo assets(); ?>/bootstrap-3.3.7-dist/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo assets(); ?>/fontello/css/fontello.min.css" />
        <link rel="stylesheet" href="<?php echo assets(); ?>/css/colorbox.css" />
        
		<script src="assets/js/jquery-1.12.3.min.js"></script>
		<script src="assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
		<script src="assets/js/jquery.colorbox-min.js"></script>
		<script src="assets/js/cycle2.js"></script>
		<script src="assets/js/swipe.min.js"></script>
		<script src="assets/js/stellar/jquery.stellar.min.js"></script>
        <script>
            jQuery(document).ready(function ($) {
                $('.nav-tabs').tab();
            });
        </script>
		<script src="assets/js/scripts.min.js<?php echo versioning()?>"></script>
    </body>
</html>
