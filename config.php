<?php

// Ścieżka definująca katalog główny zasobów.
function assets() {
    return '/new/assets';
}

// Wersjonuje zasoby, co pozwala na wymuszenie przeładowanie cache'u przeglądarki.
function versioning($version = 3) {
    return '?ver='.$version;
}