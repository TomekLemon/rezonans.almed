$(function() {
    init();
    $('#nav-trigger').on('click', null, function(e) {
       e.preventDefault(); 
       
        $('.upper-nav').fadeToggle();
    });
    $('.gallery a').colorbox({
        maxWidth: '80%',
        maxHeight: '80%',
        resize: true
    });
    
});

//    Wywołania
function init() {
    checkSize();
    initMap();
    showGallery();
    animateIcons();
    onScroll();
    scrollTo();
    addStickyClass();
}

/* To jest fix dla menu mobilnego podczas skalowania.
 * Sprawdza aktualną szerokość, gdy user skaluje okno.
 * Nie wiem czy jest to sensowne i należy do dobrej praktyki. Ale fix jest ;) 
 */
$(window).resize(function() {
    var actualWidth = $(window).width();
    if (actualWidth > 1051) {
        $('.upper-nav').show();
    } else {
        $('.upper-nav').hide();
    }
});

// Plugin paralaksy
function stellar() {
    $(document).scroll(function() {
        $.stellar({
            horizontalScrolling: false,
            verticalScrolling: true,
            verticalOffset: -1000
        });
    });
}
// Sprawdza rozmiar okna podczas skalowania.
function checkSize() {
    var width = $(window).width();
    $(window).resize(function(){
        return width;
    });
    return width;
}

// Pokazuje galerię po scrollu do niej
function showGallery() {
    $(window).scroll(function() {
        var items = $('.gallery li');
        var distance = $(window).scrollTop(); 
        var timeout = 0;

        if (distance > 700) {
            $.each(items, function (index, item) {
                setTimeout(function () {
                    $(item).attr('src', $(item).attr('src'));
                    window.parent.$(item).css('opacity', 1);
                }, timeout);

                timeout += 350;
            });
        }
    });
}

// Funkcja Google Map
function initMap() {
    var gorlice = {lat: 49.650723, lng:  21.162965};
    var przeworsk = {lat: 50.064841, lng: 22.478884};
    var map1 = new google.maps.Map(document.getElementById('gorlice'), {
        zoom: 15,
        center: gorlice,
        scrollwheel: false
        
    });
    var map2 = new google.maps.Map(document.getElementById('przeworsk'), {
        zoom: 15,
        center: przeworsk,
        scrollwheel: false
    });
    var marker1 = new google.maps.Marker({
        position: gorlice,
        map: map1
    });
    var marker2 = new google.maps.Marker({
        position: przeworsk,
        map: map2
    });
}

// Animowanie ikon
function animateIcons() {
    $(window).scroll(function() {
        var icons = $('.icon-container');
        var distance = $(window).scrollTop(); 
        var timeout = 0;

        if (distance > 1800) {
            $.each(icons, function (index, icon) {
                setTimeout(function () {
                    $(icon).attr('src', $(icon).attr('src'));
                    window.parent.$(icon).css('opacity', 1);
                }, timeout);

                timeout += 300;
            });
        }
    });
}

// Scroll po kliknięciu w menu
function scrollTo() {
    $(document).on("scroll", onScroll);
    
    //smoothscroll
    $('.upper-nav a[href^="#"], .slider-section a').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        var width = checkSize();
        
        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');
      
        var distance = $(window).scrollTop(); 
        var target = this.hash,
        menu = target;
        $target = $(target);
        if (target == '#pracownie') {
            $('body, html').animate({
                scrollTop: $(target).offset().top -120
            }, 600);
        } else {
            if (distance < 30) {
                $('body, html').animate({
                    scrollTop: $(target).offset().top -240
                }, 600);
            } else {
                $('body, html').animate({
                    scrollTop: $(target).offset().top -80
                }, 600);
            }
        }
        if (width < 1050) {
            $('.upper-nav').fadeOut()();
        }
    });
}

// "Przykleja" menu po scrollu
function addStickyClass() {
    $(window).scroll(function() {
        var width = checkSize();
        var distance = $(window).scrollTop(); 
        if (distance > 0) {
            $('.brand-section').addClass('sticky');
            if (width < 1050) {
                $('.slider-section').css('margin-top', '135px');
            } else {
                $('.slider-section').css('margin-top', '75px');
            }
        } else {
            $('.brand-section').removeClass('sticky');
            if (width < 1050) {
                $('.slider-section').css('margin-top', '0');
            } else {
                $('.slider-section').css('margin-top', '0');
            }
        }
    });
}

// Zmiana klasy podczas scrollowania
function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('.upper-nav a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement == '#kontakt') {
            var elementPos = refElement.position().top + 320;
        } else if (refElement == '#witamy') {
            var elementPos = refElement.position().top;
        } else {
            var elementPos = refElement.position().top - 280;
        }
        if (elementPos <= scrollPos && elementPos + refElement.height() > scrollPos) {
            $('.upper-nav .active').removeClass("active");
            currLink.addClass("active");
        }
        else{
            currLink.removeClass("active");
        }
    });
}